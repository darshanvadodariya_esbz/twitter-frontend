import { Routes, Route, Navigate } from "react-router-dom";
import React from 'react';
import Main from "./routes/main/main.component";
import SignInForm from "./components/signInFormComponent"
import SignUpForm from "./components/signUpFormComponent";
import Home from "./routes/home/home.component";
import Profile from "./components/profile/profile.component";

const App = () => {
  const user = localStorage.getItem('Authorization');

  return (
    <Routes>
      {user && <Route  element={<Main />}>
        <Route index  element={<Home />} />
        <Route path="/profile" exact element={<Profile />} />
      </Route>}

      <Route path="/signup" exact element={<SignUpForm />} />
      <Route path="/signin" exact element={<SignInForm />} />
      {/* <Route path="/home" exact element={<Main><Home /></Main>} /> */}
      <Route path="/signin" element={<Navigate replace to="/" />} />
    </Routes>
  );
};

export default App;