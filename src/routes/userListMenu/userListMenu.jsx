import React, { useState } from "react";
import ListUser from "../../components/list-user/list-user.component";
import "./userListMenu.styles.css";
import { Layout } from "antd";
const { Sider } = Layout;

const UserListMenu = () => {
  const [collapsed, isCollapsed] = useState(false);

  const onCollapse = (collapsed) => {
    console.log(collapsed);
    isCollapsed(collapsed);
  };

  return (
    <Sider
      className="sider-user"
      collapsible
      collapsed={collapsed}
      onCollapse={onCollapse}
    >
      <div className="logo" />
      <ListUser />
    </Sider>
  );
};

export default UserListMenu;
