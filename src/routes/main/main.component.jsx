import React from "react";
import "./main.styles.css";
import { Layout } from "antd";
import SideMenu from "../menu/menu.component";
import Header from "../header/header.component";
import UserListMenu from "../userListMenu/userListMenu";
import { Outlet } from "react-router-dom";
const { Content } = Layout;

const Main = () => {
  return (
    <Layout>
      <SideMenu />
      <Layout>
        <Header />
        <Content style={{ margin: "20px 16px 0" }}>
          {/* margin: 20px 16px 16px 220px; */}
          <Outlet />
        </Content>
      </Layout>
      <UserListMenu />
    </Layout>
  );
};

export default Main;
