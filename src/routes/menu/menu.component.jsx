import React from "react";
import "./menu.styles.css";
import { Layout, Button, Menu } from "antd";
import {
  HomeOutlined,
  BellOutlined,
  MailOutlined,
  UserOutlined,
} from "@ant-design/icons";
import logo from "../../assets/images/twitter-png-5964.png";
import TweetModal from "../../components/tweet-modal/tweetModal.component";
import { NavLink, useNavigate } from "react-router-dom";

const { Sider } = Layout;

function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

const items = [
  getItem(<NavLink to="/">Home</NavLink>, "1", <HomeOutlined />),
  getItem(<NavLink to="/">Notification</NavLink>, "2", <BellOutlined />),
  getItem(<NavLink to="/">Messages</NavLink>, "3", <MailOutlined />),
  getItem(<NavLink to="/profile">Profile</NavLink>, "4", <UserOutlined />),
];

const SideMenu = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    console.log("logout");
    localStorage.removeItem("Authorization");
    navigate("/signin");
  };

  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      // style={{ position: "fixed" }}
    >
      <div>
        <img className="logo" src={logo} alt="" />
      </div>
      <Menu
        theme="dark"
        defaultSelectedKeys={["1"]}
        mode="inline"
        items={items}
      />
      <TweetModal />
      <br />
      <br />
      <Button danger onClick={handleLogout}>
        Sign Out
      </Button>
    </Sider>
  );
};

export default SideMenu;
