import React from "react";
import "./header.styles.css"
import { Layout } from "antd";
const { Header } = Layout;

const HeaderRoute = () => {
  return (
    <Header
      className="site-layout-sub-header-background"
      style={{ padding: 0 }}
    />
  );
};

export default HeaderRoute;
