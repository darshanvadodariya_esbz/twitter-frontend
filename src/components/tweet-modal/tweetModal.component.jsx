import React, { useState } from "react";
import "./tweetModal.styles.css";
import {
  Modal,
  Button,
  Mentions,
  Form,
  Upload,
  message,
  Row,
  Col,
  Avatar,
  Comment,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { API_MAP, axiosInstance } from "../../helper/api";

const { Option } = Mentions;

function getBase64(img) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve(reader.result);
    reader.readAsDataURL(img);
    reader.onerror = (error) => reject(error);
  });
}

const TweetModal = () => {
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [, setSearch] = useState("");
  const [users, setUsers] = useState([]);
  const [fileList, setFileList] = useState([]);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");

  const showModal = () => {
    setIsModalVisible(true);
    setFileList([]);
  };

  const onSearch = (search) => {
    setSearch(search, users);
    loadGithubUsers(search);
  };

  const loadGithubUsers = async (key) => {
    try {
      const response = await axiosInstance.get(
        API_MAP.searchUser + `?name=${key}`
      );
      console.log(response.data);
      setUsers(response.data);
    } catch (error) {
      console.log(error);
    }
  };


  const handleSubmit = async (value) => {
    const formData = new FormData();
    if (value.description !== undefined) {
      formData.append("description", value.description);
    } else {
      formData.append("description", "");
    }
    formData.append("tweetMedia", fileList[0]);
    try {
      axiosInstance.post(API_MAP.tweets, formData).then((res) => {
        setIsModalVisible(false);
        console.log(res.data);
        message.success("upload successfully.");
      });
    } catch (error) {
      console.log("Error:", error);
      message.error("upload failed.");
    }
  };

  const handlePreview = async (img) => {
    if (!img.url && !img.preview) {
      img.preview = await getBase64(img.originFileObj);
    }
    setPreviewImage(img.url || img.preview);
    setPreviewVisible(true);
    setPreviewTitle(
      img.name || img.url.substring(img.url.lastIndexOf("/") + 1)
    );
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setPreviewVisible(false);
  };

  const beforeUpload = (file) => {
    setFileList([file]);
    return false;
  };

  const onRemove = () => {
    setFileList([]);
  };

  const onChange = (e) => {
    if( e.target.value.length !== 0 && e.target.value.length <= 280) {
      setFileList({ description: e.target.value.length });
    }else{
      setFileList([])
      return
    }
  };

  return (
    <>
      <Button type="primary" onClick={showModal} shape="round" size="large">
        Tweet
      </Button>

      <Modal
        visible={isModalVisible}
        destroyOnClose={true}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          form={form}
          style={{ marginTop: 10 }}
          layout="horizontal"
          preserve={false}
          onFinish={handleSubmit}
        >
          <Form.Item
            name="description"
            extra="input @ to mention people"
            rules={[
              {
                max: 280,
                message: "Tweet must be maximum 280 characters.",
              },
            ]}
            onChange={onChange}
          >
            <Mentions
              style={{ width: "100%" }}
              autoSize={{ minRows: 5, maxRows: 5 }}
              placeholder="What's happening?"
              onSearch={onSearch}
            >
              {users.map(({ name, _id, avatar, username }) => (
                <Option
                  key={_id}
                  value={name}
                  className="antd-demo-dynamic-option"
                >
                  <Comment
                    author={name}
                    avatar={
                      <Avatar
                        src={"data:image/jpeg;base64," + avatar}
                        alt={name}
                      />
                    }
                    content={<span>@{username}</span>}
                  />
                </Option>
              ))}
            </Mentions>
          </Form.Item>
          <hr />
          <Row>
            <Col span={8}>
              <Upload
                beforeUpload={beforeUpload}
                accept="image/*,video/*"
                listType="picture"
                onRemove={onRemove}
                maxCount={1}
                className="upload-list-inline"
                onPreview={handlePreview}
              >
                <Button
                  shape="circle"
                  type="dashed"
                  icon={<UploadOutlined />}
                ></Button>
              </Upload>

              <Modal
                visible={previewVisible}
                title={previewTitle}
                footer={null}
                onCancel={handleCancel}
              >
                <img
                  alt="avatar"
                  style={{ width: "100%" }}
                  src={previewImage}
                />
              </Modal>
            </Col>
            <Col span={8} offset={8}>
              <Form.Item
                shouldUpdate
                wrapperCol={{
                  offset: 12,
                }}
              >
                {() => (
                  <Button
                    type="primary"
                    htmlType="submit"
                    shape="round"
                    disabled= {fileList.length === 0}
                  >
                    Tweet
                  </Button>
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default TweetModal;
