import React from "react";
import { Upload, Button, message, Modal } from "antd";
import './uploadProfile.styles.css'
import { UploadOutlined } from "@ant-design/icons";
import { API_MAP } from "../../helper/api";

function getBase64(img) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve(reader.result);
    reader.readAsDataURL(img);
    reader.onerror = (error) => reject(error);
  });
}

class UploadProfile extends React.Component {
  state = {
    fileList: [],
    previewVisible: false,
    previewImage: "",
    previewTitle: "",
  };

  handlePreview = async (img) => {
    if (!img.url && !img.preview) {
      img.preview = await getBase64(img.originFileObj);
    }

    this.setState({
      previewImage: img.url || img.preview,
      previewVisible: true,
      previewTitle: img.name || img.url.substring(img.url.lastIndexOf("/") + 1),
    });
  };

  handleUpload = () => {
    const { fileList } = this.state;
    console.log(fileList);
    const formData = new FormData();
    fileList.forEach((file) => {
      formData.append("avatar", file);
    });

    let token = localStorage.getItem("Authorization");
    fetch(API_MAP.setAvatar, {
      method: "POST",
      headers: {
        Authorization: token,
      },
      body: formData,
    })
      .then((res) => res)
      .then(() => {
        this.setState({
          fileList: [],
        });
        message.success("upload successfully.");
        console.log("hello-1");
      })
      .catch((error) => {
        console.log(error);
        message.error("upload failed.");
      });
  };

  handleCancel = () => this.setState({ previewVisible: false });

  render() {
    const { previewVisible, previewImage, previewTitle } = this.state;
    const props = {
      beforeUpload: (file) => {
        this.setState((state) => ({
          fileList: [...state.fileList, file],
        }));
        return false;
      },
    };

    return (
      <>
        <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          maxCount={1}
          accept="image/*"
          onRemove={false}
          onPreview={this.handlePreview}
          onChange={this.handleUpload}
          {...props}
        >
          <Button
            shape="circle"
            type="dashed"
            icon={<UploadOutlined />}
          ></Button>
        </Upload>
        <Modal
          visible={previewVisible}
          title={previewTitle}
          footer={null}
          onCancel={this.handleCancel}
        >
          <img alt="avatar" style={{ width: "100%" }} src={previewImage} />
        </Modal>
      </>
    );
  }
}

export default UploadProfile;
