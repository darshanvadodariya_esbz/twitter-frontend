// import { useState } from "react";
import React, { useState, useEffect } from "react";
import moment from "moment";
import UploadProfile from "../uploadProfile/uploadProfile.component";
import {
  Row,
  Col,
  Card,
  Descriptions,
  Avatar,
  Tabs,
  Image,
  Modal,
  Button,
  Form,
  Input,
} from "antd";
import { API_MAP,axiosInstance } from "../../helper/api";

import {
  CalendarOutlined,
  BulbOutlined,
  EnvironmentOutlined,
  UserOutlined,
} from "@ant-design/icons";

import "./profile.styles.css";

import BgProfile from "../../assets/images/bg-profile.jpg";

const { TabPane } = Tabs;
const { TextArea } = Input;

const Profile = () => {
  const [form] = Form.useForm();
  const [user, setUserData] = useState({});
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isUser, isUserData] = useState({});
  
  useEffect(() => {
    const userDataGet = async () => {
      const api = API_MAP.getUser;
      try {
        const response = await axiosInstance.get(api);
        console.log(response.data);
        setUserData(response.data);
        form.setFieldsValue(response.data);
      } catch (error) {
        console.log(error);
      }
    };
    userDataGet();
  }, [form]);

  const handleSubmit = async (value) => {
    const api = API_MAP.getUser;
    try {
      const response = await axiosInstance.patch(api,value)
      const values = await form.validateFields();
      console.log("Submit:", values);
      setUserData(response.data);
      setIsModalVisible(false);
    } catch (error) {
      console.log("Error:", error);
    }
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onChange = (e) => {
    e.preventDefault();
    isUserData({ ...isUser, [`${e.target.name}`]: e.target.value });
  };

  return (
    <>
      <div
        className="profile-nav-bg"
        style={{ backgroundImage: "url(" + BgProfile + ")" }}
      ></div>
      <Card
        className="card-profile-head"
        bodyStyle={{ display: "none" }}
        title={
          <Row justify="space-between" align="middle" gutter={[24, 0]}>
            <Col span={24} md={12} className="col-info">
              <Avatar.Group>
                <Avatar
                  size={74}
                  shape="square"
                  src={<Image src={"data:image/jpeg;base64," + user.avatar} />}
                />
                <div className="profile-info">
                  <h4 className="font-semibold m-0">{user.name}</h4>
                  <p>@{user.username}</p>
                </div>
              </Avatar.Group>
            </Col>
            <Col
              span={24}
              md={12}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <Button onClick={showModal} shape="round">
                Edit Profile
              </Button>
            </Col>
            <div className="profile-info">
              <Descriptions>
                <Descriptions.Item>{user.bio}</Descriptions.Item>
              </Descriptions>
            </div>
            <div className="profile-info">
              <Descriptions>
                <Descriptions.Item label={<EnvironmentOutlined />}>
                  {user.location ? user.location : "Select Location"}
                </Descriptions.Item>
                <Descriptions.Item label={<BulbOutlined />}>
                  Born {moment(user.dob).format("MMMM DD, YYYY")}
                </Descriptions.Item>
                <Descriptions.Item label={<CalendarOutlined />}>
                  Joined {moment(user.createdAt).format("MMMM YYYY")}
                </Descriptions.Item>
                <Descriptions.Item>156 Following</Descriptions.Item>
                <Descriptions.Item>156 Following</Descriptions.Item>
              </Descriptions>
            </div>
          </Row>
        }
      ></Card>

      <Row gutter={[24, 0]}>
        <Col span={24} md={24} className="mb-24 ">
          <Card className="card-twtList">
            <Tabs defaultActiveKey="1" centered>
              <TabPane tab="Tweets" key="1">
                Content of Tweets
              </TabPane>
              <TabPane tab="Tweets & replies" key="2">
                Content of Tweets & replies
              </TabPane>
              <TabPane tab="Likes" key="3">
                Content of Likes
              </TabPane>
            </Tabs>
          </Card>
        </Col>
      </Row>

      <Modal
        forceRender
        visible={isModalVisible}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          form={form}
          style={{ marginTop: 10 }}
          layout="horizontal"
          name="editProfile"
          initialValues={{ name: "" }}
          onFinish={handleSubmit}
        >
          <center>
            <Avatar
              name="avatar"
              style={{ marginBottom: 30 }}
              size={{
                xs: 24,
                sm: 32,
                md: 40,
                lg: 64,
                xl: 80,
                xxl: 100,
              }}
              src={<Image src={"data:image/jpeg;base64," + user.avatar} />}
              icon={<UserOutlined />}
            />
          </center>

          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your Full Name!",
                whitespace: true,
              },
            ]}
          >
            <Input
              onChange={onChange}
              name="name"
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Full Name"
            />
          </Form.Item>
          <Form.Item
            name="bio"
            rules={[
              {
                message: "Please input your Bio!",
                whitespace: true,
              },
            ]}
          >
            <TextArea
              showCount
              maxLength={160}
              autoSize={{ maxRows: 3 }}
              placeholder="Bio"
              allowClear
            />
          </Form.Item>
          <Form.Item
            name="location"
            rules={[
              {
                message: "Please input your Location!",
                whitespace: true,
              },
            ]}
          >
            <Input
              prefix={<EnvironmentOutlined className="site-form-item-icon" />}
              placeholder="Location"
            />
          </Form.Item>
          <hr />
          <Row>
            <Col span={8}>
              <Form.Item label="Upload Profile">
                <UploadProfile />
              </Form.Item>
            </Col>
            <Col span={8} offset={8}>
              <Form.Item
                shouldUpdate
                wrapperCol={{
                  offset: 12,
                }}
              >
                {() => (
                  <Button
                    type="primary"
                    htmlType="submit"
                    shape="round"
                    // disabled={
                    //   !form.isFieldsTouched(true) ||
                    //   !!form
                    //     .getFieldsError()
                    //     .filter(({ errors }) => errors.length).length
                    // }
                  >
                    Save
                  </Button>
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default Profile;
