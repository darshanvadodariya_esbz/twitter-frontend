import "../App.css";
import { Form, Input, Button, Row, Col, Card } from "antd";
import axios from "axios"
import React from "react"
import { UserOutlined, LockOutlined,EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import { API_MAP } from "../helper/api";
import { useNavigate } from "react-router-dom";

const SignInForm = () => {
  const navigate = useNavigate();
  const handleSubmit = async (value) => {
  	try {
  		axios.post(API_MAP.login, value).then((res) => {
      localStorage.setItem('Authorization', (res.data.token));
      console.log(res.data);
      navigate('/');
      // window.location = "/";
    });
  	} catch (error) {
      console.log(error)
  	}
  };

  return (
    <Row
      type="flex"
      justify="center"
      align="middle"
      style={{ minHeight: "100vh" }}
    >
      <Col span={4}>
        <Card title="Sign In" style={{ width: 300, textAlign:"center" }}>
          <Form
            name="signIn"
            className="login-form"
            onFinish={handleSubmit}
          >
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your Email!",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Email"
              />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
              ]}
            >
              <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Sign In
              </Button>
            </Form.Item>
            
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default SignInForm;
