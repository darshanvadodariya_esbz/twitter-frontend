import React, { useState, useEffect } from "react";
import "./list-user.styles.css";
import { List, Avatar, Skeleton, Divider, Card,Button } from "antd";
import InfiniteScroll from "react-infinite-scroll-component";

const ListUser = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [buttonText, setButtonText] = useState('Follow');

  const handleClick = () => {
    setButtonText('Unfollow');
  }


  const loadMoreData = () => {
    if (loading) {
      return;
    }
    setLoading(true);
    fetch(
      "https://randomuser.me/api/?results=10&inc=name,gender,email,nat,picture&noinfo"
    )
      .then((res) => res.json())
      .then((body) => {
        setData([...data, ...body.results]);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadMoreData();
  // eslint-disable-next-line
  }, []);

  return (
    <>
      <Card hoverable style={{ margin: 20, marginTop: 40 }} className="ul-card">
        <div
          id="scrollableDiv"
          className="scrollHide"
          style={{
            height: 500,
            overflow: "auto",
            // padding: "0 16px",
          }}
        >
          <InfiniteScroll
            dataLength={data.length}
            next={loadMoreData}
            hasMore={data.length < 50}
            loader={<Skeleton avatar paragraph={{ rows: 1 }} active />}
            endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
            scrollableTarget="scrollableDiv"
          >
            <List
              dataSource={data}
              renderItem={(item) => (
                <List.Item key={item.id}>
                  <List.Item.Meta
                    avatar={<Avatar src={item.picture.large} />}
                    title={<a href="https://ant.design">{item.name.title}</a>}
                    description={<span>@{item.name.first}</span>}
                  />
                  <div>
                    {" "}
                    <Button
                      type="primary"
                      shape="round"
                      size={"small"}
                      onClick={handleClick}
                    >
                      {buttonText}
                    </Button>
                  </div>
                </List.Item>
              )}
            />
          </InfiniteScroll>
        </div>
      </Card>
    </>
  );
};

export default ListUser;
