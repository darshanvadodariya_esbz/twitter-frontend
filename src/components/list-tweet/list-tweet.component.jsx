import React, { useState, useEffect } from "react";
import "./list-tweet.styles.css";
import { List, Image, Avatar, Skeleton, Divider } from "antd";
import {
  MessageOutlined,
  HeartOutlined,
  RetweetOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import InfiniteScroll from "react-infinite-scroll-component";

const ListTweet = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  const loadMoreData = () => {
    if (loading) {
      return;
    }
    setLoading(true);
    fetch(
      "https://randomuser.me/api/?results=10&inc=name,gender,email,nat,picture&noinfo"
    )
      .then((res) => res.json())
      .then((body) => {
        setData([...data, ...body.results]);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadMoreData();
    // eslint-disable-next-line
  }, []);

  return (
    <div id="scrollableDiv" className="scrollHide">
      <InfiniteScroll
        dataLength={data.length}
        next={loadMoreData}
        hasMore={data.length < 60}
        loader={<Skeleton avatar paragraph={{ rows: 1 }} active />}
        endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
        scrollableTarget="scrollableDiv"
      >
        <List
          itemLayout="vertical"
          size="large"
          dataSource={data}
          renderItem={(item) => (
            <List.Item
              key={item.id}
              actions={[
                <span className="antdi-msg">
                  <MessageOutlined style={{ marginRight: 10,marginLeft:30 }} />
                  106
                </span>,
                <span className="antdi-ret">
                  <RetweetOutlined style={{ marginRight: 8 }} />
                  19
                </span>,
                <span className="antdi-like">
                  <HeartOutlined style={{ marginRight: 8 }} />
                  191
                </span>,
                <span className="antdi-share">
                  <UploadOutlined style={{ marginRight: 8 }} />
                </span>,
              ]}
            >
              <List.Item.Meta
                avatar={<Avatar src={item.picture.large} />}
                title={
                  <>
                    <a style={{ marginRight: 8 }} href="/">
                      {item.name.first} {item.name.last}
                    </a>
                    <span style={{ marginRight: 8, color: "gray" }}>
                      @{item.name.first}
                    </span>
                    <span style={{ marginRight: 8, color: "gray" }}>May 9</span>
                  </>
                }
                description={
                  <div style={{ display: "grid",color:"white" }}>
                    {item.email}
                    <Image
                    className="test"
                      width={200}
                      src={item.picture.large}
                    />
                  </div>
                }
              />
            </List.Item>
          )}
        />
      </InfiniteScroll>
    </div>
  );
};

export default ListTweet;
