import axios from "axios";
const baseURL = 'http://localhost:8001'
const token = localStorage.getItem('Authorization');

export const axiosInstance = axios.create({
  baseURL,
  headers: {
    Authorization: token,
  },
})

export const API_MAP = {
  login: baseURL + '/users/login',
  signUp: baseURL + '/users',
  getUser: '/users/me',
  setAvatar: '/users/me/avatar',
  tweets: '/tweets',
  searchUser: '/users/search',
}
